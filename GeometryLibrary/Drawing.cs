﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using MathLibrary;
using Point = MathLibrary.Point;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Serialization;

namespace GeometryLibrary
{
    /// <summary>
    /// Class for a drawing containing and drawing different types of curves.
    /// </summary>
    public class Drawing
    {
        private readonly List<Curve> _curves = new List<Curve>();
        //private List<Curve> _curves = new List<Curve>();

        public event EventHandler Redraw;

        /// <summary>
        /// The curves of the drawing as <see cref="IReadOnlyList&lt;Point&gt;"/>.
        /// </summary>
        public IReadOnlyList<Curve> Curves => _curves.AsReadOnly();

        /// <summary>
        /// Adds the passed curve to the drawing.
        /// </summary>
        /// <param name="newCurve">The curve to add.</param>
        public void AddCurve(Curve newCurve)
        {
            _curves.Add(newCurve);

            if (Redraw != null)
            {
                Redraw(newCurve, new EventArgs());
            }
        }

        /// <summary>
        /// Removes the curve at the given index from the drawing.
        /// </summary>
        /// <param name="index">The index where the curve will be removed.</param>
        public void RemoveCurve(int index)
        {
            Curve curve = _curves.ElementAt(index);

            _curves.RemoveAt(index);

            if (Redraw != null)
            {
                Redraw(curve, new EventArgs());
            }
        }

        /// <summary>
        /// Draws all contained curves.
        /// </summary>
        /// <param name="g">The graphics context to be used.</param>
        public void Draw(Graphics g)
        {
            foreach (var curve in _curves)
            {
                curve.Draw(g);
            }
        }

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="curves">The curves to be contained in the drawing.</param>
        public Drawing(Curve[] curves)
        {
            _curves.AddRange(curves);
        }

        public CurveContainer<Line> GetLines()
        {
            CurveContainer<Line> container = new CurveContainer<Line>();
            container.AddRange(_curves.Where(element => element.GetType() == typeof(Line)).Select(line => line as Line));
            return container;
        }

        public CurveContainer<Circle> GetCircles()
        {
            CurveContainer<Circle> container = new CurveContainer<Circle>();
            container.AddRange(_curves.Where(element => element.GetType() == typeof(Circle)).Select(circle => circle as Circle));
            return container;
        }

        public CurveContainer<Polyline> GetPolylines()
        {
            CurveContainer<Polyline> container = new CurveContainer<Polyline>();
            container.AddRange(_curves.Where(element => element.GetType() == typeof(Polyline)).Select(polyline => polyline as Polyline));
            return container;
        }

        //Übung 6:
        public void RemoveAllCurves()
        {
            Curve[] BeforeClear = _curves.ToArray();
            _curves.Clear();
            Redraw?.Invoke(BeforeClear, EventArgs.Empty);

            //Probably We send array(beforeClear) as sender, for VPL evaluation only 
            //Actually need only clear() drawing and update new status of drawing with redraw invoke (null as sender works well)
            //Redraw?.Invoke(null, EventArgs.Empty);
            //Redraw?.Invoke(_curves, EventArgs.Empty);
            //Redraw?.Invoke(_curves.ToArray(), EventArgs.Empty);

            //Line aLine = new Line(new Point(100, 200), new Point(400, 500));
            //this.AddCurve(aLine);
            //_curves.Add(aLine);

        }

        public void Save(string fileName)
        {
            //Method 1:
            //string ajson = JsonConvert.SerializeObject(_curves, Formatting.Indented, new JsonSerializerSettings
            //{
            //    TypeNameHandling = TypeNameHandling.Objects,
            //});
            //File.WriteAllText(fileName, ajson);

            //Method 2:
            var serializer = new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };
            //using (TextWriter writer = File.CreateText(fileName))
            using (StreamWriter writer = File.CreateText(fileName))
            {
                serializer.Serialize(writer, _curves);
            }

        }

        public void Load(string fileName)
        {
            _curves.Clear();

            //Method 1:
            var serializer = new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };
            using (StreamReader reader = File.OpenText(fileName))
           
            {
                //Curve[] atest = serializer.Deserialize(reader, typeof(Curve[])) as Curve[];
                //_curves.AddRange(atest);
                _curves.AddRange(serializer.Deserialize(reader, typeof(Curve[])) as Curve[]);
            }

            //Method 2:
            //string aString = File.ReadAllText(fileName);
            //Curve[] aTest = JsonConvert.DeserializeObject<Curve[]>(aString, new JsonSerializerSettings
            //{
            //    //Formatting = Formatting.Indented, 
            //    TypeNameHandling = TypeNameHandling.Auto
            //}) as Curve[];
            //_curves.AddRange(aTest);


            Redraw?.Invoke(_curves.ToArray(), EventArgs.Empty);
            //Redraw?.Invoke(aTest, EventArgs.Empty);

        }
    }
}
